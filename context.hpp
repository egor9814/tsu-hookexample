//
// Created by egor9814 on 11 Mar 2021.
//

#ifndef HOOK_EXAMPLE_CONTEXT_HPP
#define HOOK_EXAMPLE_CONTEXT_HPP

class Context {
public:
	enum class Theme {
		Light = 0,
		Dark,
		Black,

		Default = Dark,
	};

private:
	Theme mTheme;

public:
	Context();

	Context(const Context &c);
	Context(Context &&c) noexcept;
	Context &operator=(const Context &c);
	Context &operator=(Context &&c) noexcept;

	void setTheme(Theme theme);
	Theme getTheme() const;
};

#endif //HOOK_EXAMPLE_CONTEXT_HPP
