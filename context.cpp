//
// Created by egor9814 on 11 Mar 2021.
//

#include "context.hpp"

Context::Context() : mTheme(Theme::Default) {}

Context::Context(const Context &c) = default;

Context::Context(Context &&c) noexcept : mTheme(c.mTheme) {}

Context &Context::operator=(const Context &c) {
	if (this != &c) {
		mTheme = c.mTheme;
	}
	return *this;
}


Context &Context::operator=(Context &&c) noexcept {
	if (this != &c) {
		mTheme = c.mTheme;
	}
	return *this;
}

void Context::setTheme(Context::Theme theme) {
	mTheme = theme;
}

Context::Theme Context::getTheme() const {
	return mTheme;
}
