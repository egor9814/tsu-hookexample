#include <iostream>
#include "chart.hpp"

int main() {
	std::cout << "Hook example" << std::endl;

	Chart defaultChart;
	defaultChart.print(); // "Dark theme", because Theme::Default = Theme::Dark

	BlackChart blackChart;
	blackChart.print(); // "Black theme"

	auto ctx = std::make_shared<Context>();
	ThemedChart themedChart{ctx};
	themedChart.print(); // "Dark theme", because Theme::Default = Theme::Dark
	ctx->setTheme(Context::Theme::Light);
	themedChart.print(); // "Light theme"

	return 0;
}
