//
// Created by egor9814 on 11 Mar 2021.
//

#include "chart.hpp"
#include <iostream>

Chart::Chart() = default;

void Chart::print() {
	using namespace std;
	auto t = getTheme();
	switch (t) {
		case Context::Theme::Light:
			cout << "Light theme" << endl;
			break;
		case Context::Theme::Dark:
			cout << "Dark theme" << endl;
			break;
		case Context::Theme::Black:
			cout << "Black theme" << endl;
			break;
		default:
			cout << "Unsupported theme (code: " << static_cast<unsigned int>(t) << ")" << endl;
			break;
	}
}

Context::Theme Chart::getTheme() const {
	return Context::Theme::Default;
}

BlackChart::BlackChart() = default;

Context::Theme BlackChart::getTheme() const {
	return Context::Theme::Black;
}

ThemedChart::ThemedChart(std::shared_ptr<Context> context) : mCtx(std::move(context)) {}

Context::Theme ThemedChart::getTheme() const {
	return mCtx->getTheme();
}
