//
// Created by egor9814 on 11 Mar 2021.
//

#ifndef HOOK_EXAMPLE_CHART_HPP
#define HOOK_EXAMPLE_CHART_HPP

#include "context.hpp"
#include <memory>

class Chart {
public:
	Chart();

	void print();

protected:
	virtual Context::Theme getTheme() const;
};

class BlackChart : public Chart {
public:
	BlackChart();

protected:
	Context::Theme getTheme() const override;
};

class ThemedChart : public Chart {
	std::shared_ptr<Context> mCtx;

public:
	explicit ThemedChart(std::shared_ptr<Context> context);

protected:
	Context::Theme getTheme() const override;
};

#endif //HOOK_EXAMPLE_CHART_HPP
